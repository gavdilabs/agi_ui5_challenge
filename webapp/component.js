sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/resource/ResourceModel",
    "./controller/HelloDialog",
    "sap/f/library"
], function (UIComponent, JSONModel, ResourceModel, HelloDialog, fioriLibrary) {
    "use strict";
    return UIComponent.extend("sap.ui.demo.walkthrough.Component", {
        
        metadata: {
            manifest: "json"
        },

        init: function () {
            // call the init function of the parent
            var oModel,
                oProductsModel,
                oRouter;
            var oCustomerModel;
            var oCompanyModel;
            var oPurchaseModel;
            oModel = new JSONModel();
			this.setModel(oModel);
            UIComponent.prototype.init.apply(this, arguments);
            

            // set dialog
            //this._helloDialog = new HelloDialog(this.getRootControl());
            oRouter = this.getRouter();
			oRouter.attachBeforeRouteMatched(this._onBeforeRouteMatched, this);
			oRouter.initialize();
            oCustomerModel = new JSONModel(sap.ui.require.toUrl("sap/ui/demo/walkthroguh/db")+ ("/Customer.json"));
        },

        exit: function () {
            this._helloDialog.destroy();
            delete this._helloDialog;
        },

        openHelloDialog : function () {
            this._helloDialog.open();
        },
        _onBeforeRouteMatched: function(oEvent) {
			var oModel = this.getModel(),
				sLayout = oEvent.getParameters().arguments.layout;

			// If there is no layout parameter, set a default layout (normally OneColumn)
			if (!sLayout) {
				sLayout = fioriLibrary.LayoutType.OneColumn;
			}

			oModel.setProperty("/layout", sLayout);
		}

    });
});