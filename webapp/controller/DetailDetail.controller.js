sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/mvc/Controller"
], function (JSONModel, Controller) {
	"use strict";

	return Controller.extend("sap.ui.demo.walkthrough.controller.DetailDetail", {
		onInit: function () {
			var oOwnerComponent = this.getOwnerComponent();

			this.oRouter = oOwnerComponent.getRouter();
			this.oModel = oOwnerComponent.getModel();
			
			this.oRouter.getRoute("detailDetail").attachPatternMatched(this._onPatternMatch, this);
		},

		_onPatternMatch: function (oEvent) {
			this._supplier = oEvent.getParameter("arguments").supplier || this._supplier || "0";
			this._product = oEvent.getParameter("arguments").product || this._product || "0";

			this.getView().bindElement({
				path: "/Purchase/" + this._supplier,
				model: "purchase"
			});
            var customerID= this.getView().getModel("purchase").getProperty("/Purchase/" + this._supplier + "/customer");
            

            var myTable = this.getView().byId("customerTable");

            var oTableSearchState = [new sap.ui.model.Filter("customerID", sap.ui.model.FilterOperator.EQ, customerID)];
            //alert("id " + customerID);
            myTable.getBinding("items").filter(oTableSearchState, "Application");
            
		},

		onExit: function () {
			this.oRouter.getRoute("detailDetail").detachPatternMatched(this._onPatternMatch, this);
		}
	});
});