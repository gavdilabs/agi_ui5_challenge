sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/f/library",
	"sap/m/MessageBox",
	"sap/ui/core/Fragment"
], function (Controller, fioriLibrary, MessageBox, Fragment) {
	"use strict";

	return Controller.extend("sap.ui.demo.walkthrough.controller.Detail", {

        onInit: function () {
			var oOwnerComponent = this.getOwnerComponent();
			this.oRouter = oOwnerComponent.getRouter();
			this.oModel = oOwnerComponent.getModel();
			this.oRouter.getRoute("master").attachPatternMatched(this._onProductMatched, this);
			this.oRouter.getRoute("detail").attachPatternMatched(this._onProductMatched, this);
			this.oRouter.getRoute("detailDetail").attachPatternMatched(this._onProductMatched, this);
			this.purchase ={
				fistName:"",
				lastName:"",
				createdAt:"",
				orderConfirmationNeeded:0,





			}

		},

		

		_onProductMatched: function (oEvent) {

			this._product = oEvent.getParameter("arguments").product || this._product || "0";
			this.getView().bindElement({
				path: "/Product/" + this._product,
				model: "product"
			});

            var productID = this.getView().getModel("product").getProperty("/Product/" + this._product + "/productID");
			
			

			var myTable = this.getView().byId("suppliersTable");

			//var anotherTable = this.getView().byId("attempt");


			var oTableSearchState = [new sap.ui.model.Filter("product", sap.ui.model.FilterOperator.EQ, productID)];
            myTable.getBinding("items").filter(oTableSearchState, "Application");



			//var purchaseID = this.getView().getModel("company").getProperty("/Company/companyName");
			//alert("id" + purchaseID);
			
			
			//var purchaseID = this.getView().getModel("purchase").getProperty("/Purchase/" + this._product + "/product");
			//var myTable = this.getView().byId("suppliersTable")
			//myTable.bindElement("/purchase");
			//myTable.addContent(new Text())
			//var filter = new Filter("product")
			
		},

		onSupplierPress: function (oEvent) {
			var supplierPath = oEvent.getSource().getBindingContext("purchase").getPath(),
				supplier = supplierPath.split("/").slice(-1).pop();
				
			this.oRouter.navTo("detailDetail", {layout: fioriLibrary.LayoutType.ThreeColumnsMidExpanded, supplier: supplier, product: this._product});
		},

		onEditToggleButtonPress: function() {
			var oObjectPage = this.getView().byId("ObjectPageLayout"),
				bCurrentShowFooterState = oObjectPage.getShowFooter();
			
			oObjectPage.setShowFooter(!bCurrentShowFooterState);
		},

		onAdd: function (oEvent) {
			//var oModel= new sap.ui.model.json.JSONModel();
			//this.newPurchaseDialog.setModel(oModel);
			//this.newPurchaseDialog.getModel().setData(this.purchase);
			//this.getOwnerComponent().getModel("customer");
			if(!this.newPurchaseDialog){
			this.newPurchaseDialog = sap.ui.xmlfragment("sap.ui.demo.walkthrough.view.register",this);
			//var oModel= new sap.ui.model.json.JSONModel();
			//this.newPurchaseDialog.setModel(oModel);
			
			}
			//this.newPurchaseDialog.getModel().setData(this.purchase);
			this.newPurchaseDialog.open();
		},

		handleCancelButtonPress: function(){
			this.newPurchaseDialog.close();
		},


        onExit: function () {

			this.oRouter.getRoute("master").detachPatternMatched(this._onProductMatched, this);
			this.oRouter.getRoute("detail").detachPatternMatched(this._onProductMatched, this);
		},

        
	});
});