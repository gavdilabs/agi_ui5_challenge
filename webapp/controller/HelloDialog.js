sap.ui.define([
    "sap/ui/base/ManagedObject",
    "sap/ui/core/Fragment"
], function (ManagedObject, Fragment) {
    "use strict"

    return ManagedObject.extend("sap.ui.walkthrough.controller.register", {

        onInit: function () {

			// set explored app's demo model on this sample
			var oModel = this.getOwnerComponent().getModel("customer");
			this.getView().setModel(oModel);
		},

        constructor: function (oView) {
            this._oView = oView
        },

        exit: function () {
            delete this._oView;
        },

    });
});